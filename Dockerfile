FROM ubuntu:18.04
RUN apt-get update -y
RUN apt-get install zsh git wget tree -y
RUN chsh -s /usr/bin/zsh root
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
RUN cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
CMD tail -f /dev/null
