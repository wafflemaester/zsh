# zsh
How to build
```bash
docker build -t zsh-sandbox:latest .
```

How to run
```bash¬
docker run -d --rm --name=zsh_container zsh-sandbox:latest
```

How to log on
```bash¬¬
docker exec -it zsh_container zsh
```

How to log out of the container
```¬
^d
```

How to stop the conteiner
```bash¬¬
docker stop zsh_container
```
